import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()
class Common:
    id = sa.Column(sa.Integer, primary_key=True)


#сотрудники
class Bartender(Common, Base):
    __tablename__ = 'bartender'
    
    name = sa.Column(sa.String, nullable=False)
    salary = sa.Column(sa.Integer, nullable=False)


class Storekeeper(Common, Base):
    __tablename__ = 'storekeeper'
    
    name = sa.Column(sa.String, nullable=False)
    salary = sa.Column(sa.Integer, nullable=False)

#товар - склад
class Product(Common, Base):
    __tablename__ = 'product'
    
    name = sa.Column(sa.String, nullable=False)
    price = sa.Column(sa.Integer, nullable=False)
    count = sa.Column(sa.Integer, nullable=False)
    

#продажа
class Sale(Common, Base):
    __tablename__ = 'sale'

    bartender_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Bartender.__tablename__}.id"), nullable=False)
    #product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    product = relationship(Product, uselist=True)
    count = sa.Column(sa.Integer, nullable=False, default=0)
    price = sa.Column(sa.Integer, nullable=False)


#поставка
class Delivery(Common, Base):
    __tablename__ = 'delivery'

    storekeeper_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Storekeeper.__tablename__}.id"), nullable=False)
    supplier = sa.Column(sa.String, nullable=False)
    #product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    product = relationship(Product, uselist=True)
    count = sa.Column(sa.Integer, nullable=False, default = 0)
    price = sa.Column(sa.Integer, nullable=False)
    delivery_data = sa.Column(sa.Date, nullable=False, default=datetime.date.today)



class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite:///database2.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_schedule(self):
        return self.session.query(Speach).all()


class BartenderRole(Role):
    def register(self, name, salary):
        bartender = Bartender(name = name, salary = salary)
        self.session.add(bartender)
        self.session.commit()
        self.record = bartender

    def sale(self, name, product, count, price):
        bartender_id = self.session.query(Bartender).filter(Bartender.name == name).first()
        sale = Sale(bartender_id = bartender_id.id, product = product, count=count, price = price)
        self.session.add(sale)
        self.session.commit()
        self.record = sale


class StorekeeperRole(Role):
    def register(self, name, salary):
        storekeeper = Storekeeper(name = name, salary = salary)
        self.session.add(storekeeper)
        self.session.commit()
        self.record = storekeeper
        
    def delivery(self, name, supplier):
        storekeeper_id = self.session.query(Storekeeper).filter(Storekeeper.name == name).first()
        delivery = Delivery(bartender_id = bartender_id.id, supplier = supplier, 
                            product = product, count=count, price = price)
        self.session.add(sale)
        self.session.commit()
        self.record = sale


bartender = BartenderRole()
bartender.register(name = "Stiv")
bartender.sale(bartender_id = 1, product = "cofee", count = 2, price = "100")