import requests
import tkinter as tk
from tkinter import *


class Example(Frame):
    en_city = Entry(width=50)
    en_temp = Label(width=50)
    en_wind_sp = Label(width=50)
    en_wind_der = Label(width=50)

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.create_widgets()

    def create_widgets(self):
        self.en_city.insert(0, "Санкт-Петербург")
        self.en_city.pack()

        ok_but = Button(text="OK", command=self.start)
        ok_but.pack()

        json = self.get_json(self.en_city.get())
        temp = self.get_temp(json)
        sp = self.wind_speed(json)
        der = self.wind_direction(json)

        self.en_temp.config(text=temp)
        self.en_wind_sp.config(text=sp)
        self.en_wind_der.config(text=der)

        self.en_temp.pack()
        self.en_wind_sp.pack()
        self.en_wind_der.pack()

    def start(self):
        json = self.get_json(self.en_city.get())
        temp = self.get_temp(json)
        sp = self.wind_speed(json)
        der = self.wind_direction(json)

        self.en_temp.config(text=temp)
        self.en_wind_sp.config(text=sp)
        self.en_wind_der.config(text=der)


    def get_json(self, city_name):
        response = requests.get(
            "http://api.openweathermap.org/data/2.5/weather",
            params={
                "q": city_name,
                "units": "metric",
                "appid": "5d1c5370369029f2d3d9274729db73b2",
            }
        )
        json = response.json()
        return json


    def get_temp(self, json):
        temp = json['main']['temp']
        forecast = "Temperature: " + str(temp) + " С°"
        return forecast + "\n"

    def wind_speed(self, json):
        sp = json['wind']['speed']
        return "Wind speed: " + str(sp) + " m/s"

    def wind_direction(self, json):
        deg = json['wind']['deg']
        degree = int((deg / 22.5) + .5)
        carr = ["N", "NNE", "NE", "ENE",
                "E", "ESE", "SE", "SSE",
                "S", "SSW", "SW", "WSW",
                "W", "WNW", "NW", "NNW"]

        return "Direction of the wind: " + carr[(degree % 16)] + "\n"


def main():
    root = Tk()
    root.geometry("600x150+300+300")
    app = Example(root)
    root.mainloop()


if __name__ == '__main__':
    main()